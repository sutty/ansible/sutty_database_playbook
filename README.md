Sutty PostgreSQL Playbook
=========================

PostgreSQL Ansible playbook for Sutty.

Variables
---------

| Variable             | Description               | Default value |
| -------------------- | ------------------------- | ------------- |
| `container_version`  | Container version         | 3.17.1-15     |
| `data`               | Data directory            | /srv/sutty    |
| `registry`           | Container registry        | -             |
| `registry_username`  | Login to registry as user | -             |
| `registry_password`  | With this password        | -             |
| `mmmonit`            | Mmmonit host:port         | -             |
| `monit_email_to`     | Monit status email to     | -             |
| `monit_email_from`   | Monit status email from   | -             |
